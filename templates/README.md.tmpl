Hey, I'm {{ index .Hcard.Properties.name 0 }} ({{ index (index .Hcard.Properties "pronoun-nominative") 0 }}/{{ index (index .Hcard.Properties "pronoun-oblique") 0 }}/{{ index (index .Hcard.Properties "pronoun-possessive") 0 }}) 👋

{{ .HcardJob }}, and I'm currently based in {{ index .Hcard.Properties.locality 0 }}.

I have a [/now page]({{ AddTracking "https://www.jvt.me/now/" }}), which aims to be a more up-to-date about page.

I use my [personal website]({{ AddTracking "https://www.jvt.me/" }}) as a method of blogging about my learnings, as well as sharing information about projects I have previously, or am currently, working on in my spare time.

I maintain [a number of Open Source projects]({{ AddTracking "https://www.jvt.me/open-source/" }}), and primarily use [GitLab](https://gitlab.com/jamietanna) for my source control, but also use [GitHub](https://github.com/jamietanna) for some things.

---

I write a fair bit on my blog:

{{ range .Articles }}
- [_{{ index .Properties.name 0 }}_]({{ index .Properties.url 0 | AddTracking }})
{{- end }}

---

I blog as a form of documentation, as noted in my post [Blogumentation - Writing Blog Posts as a Method of Documentation]({{ AddTracking "https://www.jvt.me/posts/2017/06/25/blogumentation/" }}):

{{ range .Blogumentation }}
- [_{{ index .Properties.name 0 }}_]({{ index .Properties.url 0 | AddTracking }})
{{- end }}

---

I track articles and resources that I recommend I/others read [as bookmarks on my site]({{ AddTracking "https://www.jvt.me/kind/bookmarks/" }}), the latest of which are:

{{ range .Bookmarks }}
- [_{{ .Title }}_]({{ .URL | AddTracking }})
{{- end }}

---

I also write Week Notes as a way of summarising what's going on in my life. The last one can be found at [{{ index .WeekNote.name 0}}]({{ index .WeekNote.url 0 | AddTracking }}).

---

I like to track my data in [IndieWeb fashion](https://indieweb.org/why). For instance, the last book I read was {{ .LastRead }}, and yesterday, I took {{ .LastSteps }}.
